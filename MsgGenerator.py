#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar 25 16:31:22 2021

@author: gerardgg
"""

from Exceptions import MsgGeneratorNotFound

class MsgGenerator:
    
    @staticmethod
    def genMsg(type: str, msg: str) -> str:
        colorDic = {
            "HEADER": '\033[95m',
            "OKBLUE": '\033[94m',
            "OKCYAN": '\033[96m',
            "OKGREEN": '\033[92m',
            "WARNING": '\033[93m',
            "FAIL": '\033[91m',
            "ENDC": '\033[0m',
            "BOLD": '\033[1m',
            "UNDERLINE": '\033[4m'
        }
        type = type.upper()
        if type in colorDic:
            return colorDic[type] + msg + colorDic['ENDC']
        else:
            raise MsgGeneratorNotFound