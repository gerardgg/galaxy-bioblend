#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar 25 08:59:43 2021

@author: gerardgg
"""
import json, sys
from MsgGenerator import MsgGenerator
from bioblend import galaxy
from pprint import pprint
from steps import TxtParam, DataCollecParam
import re

compat_workflows = {"COVID-19: variation analysis reporting",
                    "COVID-19: variation analysis on ARTIC PE data",
                    "COVID-19: variation analysis of ARTIC ONT data",
                    "COVID-19: variation analysis on WGS SE data",
                    "COVID-19: variation analysis on WGS PE data"}
def check_type(d_type: str, value: str):
    if d_type == "integer":
        return False if value.isnumeric() else True
    elif d_type == "float":
        return False if re.match(r'^-?\d+(?:\.\d+)$', value) else True
    elif d_type == "text":
        return False if isinstance(value, str) else True
    elif d_type == "color":
        #todo
        return False

def prepare_workflow(option: str):
    workflow_params = json.loads(json.dumps(gi.workflows.show_workflow(option)))
    
    print(workflow_params)
    accepted_params = ("parameter_input", "data_input", "data_collection_input")
    param_labels = workflow_params["inputs"]
    param_data = []
    target_fields = ("id", "type", "annotation", "default")
    
    for elem in workflow_params["steps"]:
        if workflow_params["steps"][elem]["type"] in accepted_params:

           p_id: int = int(workflow_params["steps"][elem]["id"])
           print(MsgGenerator.genMsg("UNDERLINE","ID: " + str(p_id)))
           print(MsgGenerator.genMsg("BOLD", param_labels[elem]["label"]))

           print(workflow_params["steps"][elem]["annotation"])
           
           optional: bool = workflow_params["steps"][elem]["tool_inputs"]["optional"]
           
           p_type = workflow_params["steps"][elem]["type"]
           # todo: if the input is empty and the param is optional
           if p_type == accepted_params[0]:
              data_type: str = workflow_params["steps"][elem]["tool_inputs"]["parameter_type"]
              print("Data type: " + data_type)
              default: str = workflow_params["steps"][elem]["tool_inputs"]["default"]
              print("Default value: " + str(default))
              
              type_flag = True
              while type_flag:
                  param_value = input("Type the value> ")
                  type_flag = check_type(data_type, param_value)
                  if type_flag:
                      print(MsgGenerator.genMsg("FAIL", "Error: Introduce a valid type"))
           elif p_type == accepted_params[1]:
               p_format: str =  workflow_params["steps"][elem]["tool_inputs"]["format"]
               print("Format: " + str(p_format))
           elif p_type == accepted_params[2]:
               p_format: str =  workflow_params["steps"][elem]["tool_inputs"]["format"]
               print("Accepted formats: "  + str(p_format))
           
def showMenu():
    while True:
        print("1. List workflows\n" +
          "2. List histories\n" +
          "3. Run workflow\n" +
          "4. Exit")
        option = int(input(MsgGenerator.genMsg("HEADER", "Pick an option> ")))
        if option == 1:
            hl = json.loads(json.dumps(gi.workflows.get_workflows(published=True)))
            print(MsgGenerator.genMsg("WARNING", "Name\t-\tID"))
            for elem in hl:
                if elem["name"] in compat_workflows:
                    print(MsgGenerator.genMsg("BOLD", elem['name'] + '\t-\t' + elem['id']))
        elif option == 2:
            hl = json.loads(json.dumps(gi.histories.get_histories()))
            print(MsgGenerator.genMsg("WARNING", "Name\t-\tID"))
            for elem in hl:
                print(MsgGenerator.genMsg("BOLD", elem['name'] + '\t-\t' + elem['id']))
        elif option == 3:
            option = str(input(MsgGenerator.genMsg("HEADER", "Enter workflow id> ")))
            prepare_workflow(option)
        elif option == 4:
            sys.exit()
                    
                

if len(sys.argv) == 2:
    galaxy_key = sys.argv[1]
    if len(galaxy_key) == 32:
        
        gi = galaxy.GalaxyInstance(url='https://covid19.usegalaxy.es/', key= galaxy_key)
        try:
            print(MsgGenerator.genMsg(type = "OKBLUE", msg = "### COVID GALAXY CLI ###"))
            print(MsgGenerator.genMsg(type = "OKGREEN", msg = "Connected succesfuly"))
            showMenu()

        except Exception as exception:
            print(type(exception))
    else:
        print(MsgGenerator.genMsg(type = "FAIL", msg = "Incorrect galaxy key: Must be 32 char. length"))
else:
    print(MsgGenerator.genMsg(type = "FAIL", msg ="Incorrect args: python main.py [USER_KEY]"))
    