from bioblend import galaxy
import plac
import json

@plac.annotations(
    userKey=("User key of Galaxy server", 'positional', None, str, None, None),
    jsonFile=("JSON file with workflow params", 'positional', None, str, None, None))

def main(
        userKey, 
        jsonFile):
    gi = galaxy.GalaxyInstance(url='https://covid19.usegalaxy.eu', key= userKey)
    wfFile = json.loads(open(jsonFile).read())

    ''' Dictionary to store workflow inputs '''
    '''p_inputs = {}
  
    for wfInput in wfFile['inputs']:
        p_inputs[wfInput] = wfFile['inputs'][wfInput]'''

    gi.workflows.invoke_workflow(wfFile['workflow_id'], inputs= wfFile['inputs'], history_name= "test2", import_inputs_to_history= True)

if __name__ == '__main__':
    plac.call(main)