#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Mar 27 22:53:00 2021

@author: gerard
"""

from abc import ABC

class Step:
    
    def __init__(self, p_id: int, label: str, p_type: str, annotation: str, optional: bool):
        self.p_id = p_id
        self.label = label
        self.p_type = p_type
        self.annotation = annotation
        self.optional = optional
        
class DataParamStep:
    
    def __init__(self, format: tuple[str, ...]):
        self.format = format
        

class TxtParam(Step):
    
    def __init__(self, p_id: int, label: str, p_type: str, annotation: str, optional: bool, default: str, data_type: str):
        Step.__init__(self, p_id, label, p_type, annotation, optional)
        
        # Exclusive attribs.
        self.default = default
        self.data_type = data_type
        
    def __str__(self) -> str:
        return("(id:{id}|label:{label}|p_type:{p_type}|annotattion:{annotattion}|optional:{optional}|default:{default}|data_type:{data_type})".format(
            id = self.p_id, label = self.label, p_type = self.p_type, annotattion = self.annotation, optional = self.optional, default = self.default, data_type = self.data_type))
        

class DataParam(Step, DataParamStep):
    
    def __init__(self, p_id: int, label: str, p_type: str, annotation: str, optional: bool, format: tuple[str, ...]):
        
        Step.__init__(self, p_id, label, p_type, annotation, optional)
        
        # Exclusive attribs.
        DataParam.__init__(self, format)

class DataCollecParam(Step, DataParamStep):
    
    def __init__(self, p_id: int, label: str, p_type: str, annotation: str, optional: bool, collection_type: str, format: tuple[str, ...]):
        Step.__init__(self, p_id, label, p_type, annotation, optional)
        
        # Exclusive attribs.
        DataParam.__init__(self, format)
        self.collection_type = collection_type