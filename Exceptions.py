#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar 25 17:05:45 2021

@author: gerardgg
"""

class Error(Exception):
    """ Base class for other exceptions"""
    pass

class MsgGeneratorNotFound(Error):
    
    def __init__(self, message = "MsgGenerator type not found"):
        super.__init__(self.message)